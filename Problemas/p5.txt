Problema 5.Calcula el descompte
===========Descripció=====
Llegeix el preu original i el preu actual i imprimeix el descompte (en %).
===========Entrada=====
Per l’entrada rebreu dos nombres decimals on, el primer, representa el preu original i, el segon, el
preu amb descompte.
===========Sortida=====
Per la sortida sempre haureu d’imprimir un altre decimal amb el valor del descompte representat
en %.