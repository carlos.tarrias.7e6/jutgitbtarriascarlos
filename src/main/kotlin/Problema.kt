import java.io.File

/**
 * Representa cada uno de los problemas de programación del juez ITB que se le enseñarán al usuario.
 * @author Carlos Tarrias Diaz
 * @version 1.0.0
 * @see CasoPrueba
 */
class Problema(nombreFichero:String, ficheroCasos:String, var resuelto: Boolean = false, var numeroIntentos: Int = 0) {
   var enunciado = ""
   var juegosPruebaPublicos = mutableListOf<CasoPrueba>()
   var juegosPruebaPrivados = mutableListOf<CasoPrueba>()
   var listaIntentos = mutableListOf<String>()

  /**
   * Instrucciones de inicialización del objeto.
   */
  init {
    //1. Cargamos el texto del problema del archivo correspondiente en ./Problemas/
    this.enunciado = File(nombreFichero).readText()
    var casoPrueba : CasoPrueba
    //2. Cargamos todos los casos de prueba (privados y públicos) del archivo correspondiente en ./CasosPrueba
    for (caso in File(ficheroCasos).readLines()) {
      casoPrueba = CasoPrueba(caso.split(" "))
      //3.Añadimos el caso a la lista correspondiente en función de su tipo (público o privado).
      if (casoPrueba.tipo == TipoCaso.PUBLICO) this.juegosPruebaPublicos.add(casoPrueba)
      else if (casoPrueba.tipo == TipoCaso.PRIVADO) this.juegosPruebaPrivados.add(casoPrueba)
    }
  }

  /**
   * Añade la respuesta del usuario en un intento concreto a la lista de respuestas del problema.
   * @param intento String : Respuesta del usuario.
   */
  fun addIntento(intento: String) {
    this.listaIntentos.add(intento)
  }

  /**
   * Incrementa el número de veces que ha intentado responder el usuario al problema.
   */
  fun incrementarIntentos() = this.numeroIntentos++

  /**
   * Muestra la lista de casos públicos que contiene el problema (cada caso con sus entradas y salida).
   */
  fun mostraCasosPublicos() {
    this.juegosPruebaPublicos.forEach { println(it) }
  }

  /**
   * Devuelve la respuesta al problema
   * De momento suponemos que sólo existe una única respuesta y que sólo hay un caso privado a resolver.
   * @return String : Representa la respuesta al problema.
   */
  fun getRespuesta():String = this.juegosPruebaPrivados[0].outputs[0]

  /**
   * Muestra la lista de casos privados (de momento sólo uno) que contiene el problema.
   * De cada caso privado sólo se muestran sus entradas/inputs.
   */
  fun mostraCasosPrivados() {
    juegosPruebaPrivados.forEach {
      val caso = """
      | ==============CASO DE PRUEBA PRIVADO============ 
      | =====>ENTRADAS 
      | ${it.inputs}
      """.trimMargin()
      println(caso)
    }
  }

  /**
   * Sobrecarga de la representación en String del objeto Problema.
   * @return String: Representación del Problema en formato texto.
   */
  override fun toString(): String {
    return "${this.enunciado}\nINTENTOS REALIZADOS : ${this.numeroIntentos}\nRESPUESTAS ANTERIORES : ${this.listaIntentos}\n"
  }
}