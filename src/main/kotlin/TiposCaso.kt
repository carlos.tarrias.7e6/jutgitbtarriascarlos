/**
 * Representa los diferentes tipos de los casos de prueba.
 * De momento sólo tenemos casos públicos y privados.
 * @author Carlos Tarrias Diaz
 * @version 1.0.0
 * @see CasoPrueba
 */
enum class TipoCaso {
  DEFAULT,
  PUBLICO,
  PRIVADO
}