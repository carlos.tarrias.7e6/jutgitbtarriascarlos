/**
 * Punto de entrada del programa Juez ITB.
 * @author Carlos Tarrias Diaz
 * @version 1.0.0.
 */
fun main() {

  val listaProblemas = mutableListOf<Problema>()

  //Creamos la lista de problemas que tendrá el juez ITB.
  cargarProblemas(listaProblemas)

  //Vamos mostrando los problemas (con sus intentos e informando si están resueltos) en ciclo hasta que el usuario lo decida.
  mostrarProblemas(listaProblemas)

}

