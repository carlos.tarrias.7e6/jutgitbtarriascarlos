/**
 * Representa un caso de prueba que forma parte de un problema del Juez ITB. Contiene una lista de inputs y outputs y puede ser público o privado.
 * @author Carlos Tarrias Diaz
 * @version 1.0.0
 * @see TipoCaso
 */
class CasoPrueba(inputsIOutputs:List<String>) {
  var inputs = mutableListOf<String>()
  var outputs = mutableListOf<String>()
  var tipo = TipoCaso.DEFAULT

  /**
   * Instrucciones de inicialización del objeto.
   */
  init {
    // IMPORTANTE:
    // Cada línea del juego de pruebas sigue esta regla
    // cáracterVisibilidad input1 input2 (...) inputN output1
    //1. En función del carácter que indicarà la línea del archivo (V:Público , O:Privado) establecemos su tipo.
    if (inputsIOutputs[0] == "V") {
      this.tipo = TipoCaso.PUBLICO
    }
    else if (inputsIOutputs[0] == "O") {
      this.tipo = TipoCaso.PRIVADO
    }
    //2. De momento sólo hay un único output en cada casos público/privado. Que corresponde al último numero de la línea.
    this.outputs.add(inputsIOutputs.last())
    //3. El resto de números corresponden a los inputs del caso.
    inputsIOutputs.subList(1,inputsIOutputs.size-1).forEach {this.inputs.add(it)}
  }

  /**
   * Sobrecarga de la representación en String del objeto CasosPrueba.
   * @return String: Representación del caso de prueba en formato texto.
   */
  override fun toString(): String {
    val caso = """
      | ==============CASO DE PRUEBA ${this.tipo}============ 
      | =====>ENTRADAS 
      | ${this.inputs} 
      | =====>SALIDAS 
      | ${this.outputs} 
      """.trimMargin()
    return "${caso}\n"
  }
}