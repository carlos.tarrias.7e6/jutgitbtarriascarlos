import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class UtilsKtTest {

  //fun cargarProblemas() Tests
  @Test
  fun compruebaListaProblemasTengaLongitudCorrecta() {
    val testLista = mutableListOf<Problema>()
    cargarProblemas(testLista)
    assertTrue(testLista.size == 5)
  }

  @Test
  fun compruebaTodosLosProblemasEstanDefinidos() {
    val testLista = mutableListOf<Problema>()
    cargarProblemas(testLista)
    assertTrue(testLista.all { it.enunciado.isNotEmpty() && it.juegosPruebaPublicos.isNotEmpty() && it.juegosPruebaPrivados.isNotEmpty()})
  }

  @Test
  fun compruebaTodosLosProblemasAlInicioNoTenganRegistroIntentos() {
    val testLista = mutableListOf<Problema>()
    cargarProblemas(testLista)
    assertTrue(testLista.all { it.listaIntentos.isEmpty() && it.numeroIntentos == 0})
  }


  //fun mostrarProblemas() Tests{



  //fun decidirProblema() Tests {



  //fun resolverProblema() Tests

}