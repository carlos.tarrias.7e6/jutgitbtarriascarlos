# Proyecto Troncal M3 : JutgITB
## Autores
- [Carlos Tarrias Diaz](https://gitlab.com/carlos.tarrias.7e6)

## Índice
- [Objetivo](#objetivo)
- [Descripción](#descripción)
- [Funcionalidades](#Funcionalidades Versión 1.0.0.)

## Objetivo
Realizar un juez en línea de problemas de programación, inspirado en otros jueces como [Jutge](https://jutge.org/) o [Joel](https://jo-el.es/contest/testcodejam23), que permita mostrar diferentes enunciados. Con la posibilidad de 
que el usuario pueda introducir su respuesta al problema deseado.

## Descripción
Al iniciar el juez se muestra el primer problema de la lista (juntamente con el enunciado y el juego de pruebas público) y le pide al usuario si lo quiere resolver. Hay dos opciones:

 -Resolver: El sistema muestra el juego de pruebas privado (de momento sólo hay un caso privado) y pide la respuesta.
 El usuario puede intentar resolverlo tantas veces como desee.
 
 -No resolver: El sistema muestra el problema siguiente

 > Si el problema ya está resuelto se muestra el siguiente problema automáticamente.

Cuando el usuario introduce una respuesta se genera un intento, esta respuesta se registra internamente en el sistema.

La lista de problemas se sigue mostrando mientras el usuario lo indique.

## Funcionalidades Versión 1.0.0.

- **Carga de problemas y de juego de pruebas**
    - Al inicio del programa se cargan todos los problemas juntamente con todo el juego de pruebas de cada uno de éstos. 
    - Los enunciados de los problemas se encuentran en la carpeta del proyecto **__Problemas__**.
       > El nombre de los archivos es __**p{num}.txt**__ . Dónde __num__ es el número del problema. 
    - Los casos de prueba se encuentran en la carpeta del proyecto **__CasosPrueba__**.
       > El nombre de los archivos es __**cp{num}.txt**__. Dónde __num__ es el número del problema.
- **Mostrar lista de problemas**:
    - Se muestran siempre 5 problemas en el mismo orden (enunciado + juego casos público) en bucle hasta que el usuario introduzca 'n' ó 'N'.
    - Si el sistema detecta que el problema está resuelto se pone enfásis en el siguiente problema no resuelto.
- **Opción de resolver el problema**
    - Se le indica al usuario si quiere resolver el problema, si éste introduce el carácter 'S' ó 's', se le muestra el caso privado del problema.
    - El programa pide al usuario la respuesta (de momento sólo hay respuestas numéricas):
      - Si la respuesta es correcta se genera un intento y se pasa a mostrar el siguiente problema.
      - Si no es correcta se genera un intento y se le indica al usuario si quiere responder otra vez. Hay tantos intentos como el éste desee.


